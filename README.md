# PyTorch-Jacinto-AI-DevKit
Training & Quantization Tools For Embedded AI Development.

### Deprecation Notice (16 August 2021)
This repository has been deprecated. Please use the new repository: [https://github.com/TexasInstruments/edgeai-torchvision](https://github.com/TexasInstruments/edgeai-torchvision)

Also, visit our new landing page for developers at: [https://github.com/TexasInstruments/edgeai](https://github.com/TexasInstruments/edgeai)

If you still need to access the old repository/documentation, it is [here](./README_OLD.md). This will not be maintained and may be potentially removed in the future.
