#git branch -D release
#git checkout --orphan release

git stash

git checkout release

#remove --squash to keep history
git merge --allow-unrelated-histories -X theirs master --squash

# remove internal files
find . -name *internal.* -delete
find . -type d -name *internal -exec rm -rf {} +

git add -u

# commit
git commit -a -m 'release commit'

#git stash pop

