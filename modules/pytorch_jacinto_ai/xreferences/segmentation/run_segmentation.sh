#!/usr/bin/env bash

DATE_TIME=`date +'%Y-%m-%d_%H-%M-%S'`
LOG="train-segmentation-log-${DATE_TIME}.txt"
exec &> >(tee -a ${LOG})
echo Logging output to ${LOG}

#examples of models that can be trained: fcn_resnet50 deeplabv3_resnet50

python -m torch.distributed.launch --nproc_per_node=4 --use_env ./train.py --lr 0.02 --dataset cityscapes -b 4 --model fcn_resnet50 --aux-loss --epochs 100 --img_size 520 1040

python ./train.py --dataset cityscapes --model fcn_resnet50 --img_size 520 1040 --pretrained model_99.pth --test-only

python ./train.py --dataset cityscapes --model fcn_resnet50 --img_size 520 1040 --pretrained model_99.pth --export_model

python ./train.py --dataset cityscapes --model fcn_resnet50 --img_size 520 1040 --pretrained model_99.pth --complexity
