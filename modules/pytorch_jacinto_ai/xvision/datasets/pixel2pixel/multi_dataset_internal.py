#################################################################################
# Copyright (c) 2018-2021, Texas Instruments Incorporated - http://www.ti.com
# All Rights Reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#################################################################################

"""
Reference: http://sintel.is.tue.mpg.de/

Butler, D. J. and Wulff, J. and Stanley, G. B. and Black, M. J.,
A naturalistic open source movie for optical flow evaluation,
European Conf. on Computer Vision (ECCV), 2012

Lessons and insights from creating a synthetic optical flow benchmark,
Wulff, J. and Butler, D. J. and Stanley, G. B. and Black, M. J.,
ECCV Workshop on Unsolved Problems in Optical Flow and Stereo Estimation,
2012
"""

from __future__ import division
import os
import torch
import torch.utils.data as data

from .mpisintel import mpi_sintel_final
from .mpisintel import mpi_sintel_depth
from .kitti_sceneflow import kitti_occ
from .kitti_depth import kitti_depth

__all__ = ['mpi_sintel_depth_plus_kitti_depth', 'mpi_sintel_flow_plus_kitti_depth_sceneflow', 'mpi_sintel_flow_plus_kitti_occ_plus_kitti_depth_sceneflow']

class MiltiListDataset(data.Dataset):
    def __init__(self, target_channels=3, list_datasets=None, target_ids=None, transform=None):
        self.target_channels = target_channels
        self.list_datasets = list_datasets
        self.target_ids = target_ids
        self.dataset_idx = 0
        self.transform = transform

    def __getitem__(self, index):
        index = index % len(self.list_datasets[self.dataset_idx])
        inputs, target = self.list_datasets[self.dataset_idx][index]

        targets = [target]
        target_size = target.size(2)
        if target.size(2) < self.target_channels:
            if self.target_ids[self.dataset_idx] > 0:
                extra_size_before = (target.size(0), target.size(1), self.target_ids[self.dataset_idx])
                targets = targets.insert(0, (torch.zeros(extra_size_before)))
                target_size += self.target_ids[self.dataset_idx]

            if self.target_channels > target.size(2):
                extra_size_after = (target.size(0), target.size(1), self.target_channels-target.size(2))
                targets.append(torch.zeros(extra_size_after))
                target_size += self.target_channels-target.size(2)

        self.dataset_idx = ((self.dataset_idx+1) % len(self.list_datasets))

        if self.transform is not None:
                inputs, target = self.transform(inputs, targets)

        return inputs, targets

    def __len__(self):
        max_len = 0
        for l in self.list_datasets:
            max_len = max(max_len, len(l))

        return max_len



def mpi_sintel_depth_plus_kitti_depth(dataset_config, root, transforms=None):
    root_folders = root.split('+')

    mpi_sintel_depth_train, mpi_sintel_depth_test = mpi_sintel_depth(root=root_folders[0], transforms=None, split=80)

    kitti_depth_splits = [os.path.join(root_folders[1],'train.txt'), os.path.join(root_folders[1],'val.txt')]
    kitti_depth_train, kitti_depth_test = kitti_depth(root=root_folders[1],transforms=None, split=kitti_depth_splits)

    mpi_sintel_depth_plus_kitti_depth_sceneflow_train = MiltiListDataset(1, [mpi_sintel_depth_train, kitti_depth_train], [0,0], transform=transforms[0])
    mpi_sintel_depth_plus_kitti_depth_sceneflow_test = MiltiListDataset(1, [mpi_sintel_depth_test, kitti_depth_test], [0,0], transform=transforms[1])

    return mpi_sintel_depth_plus_kitti_depth_sceneflow_train, mpi_sintel_depth_plus_kitti_depth_sceneflow_test



def mpi_sintel_flow_plus_kitti_depth_sceneflow(dataset_config, root, transforms=None):
    root_folders = root.split('+')

    mpi_sintel_train, mpi_sintel_test = mpi_sintel_final(root=root_folders[0],transforms=None, split=80)

    kitti_depth_splits = [os.path.join(root_folders[1],'train.txt'), os.path.join(root_folders[1],'val.txt')]
    kitti_depth_train, kitti_depth_test = kitti_depth(root=root_folders[1],transforms=None, split=kitti_depth_splits)

    mpi_sintel_plus_kitti_depth_sceneflow_train = MiltiListDataset(3, [mpi_sintel_train,kitti_depth_train], [0,2], transform=transforms[0])
    mpi_sintel_plus_kitti_depth_sceneflow_test = MiltiListDataset(3, [mpi_sintel_test,kitti_depth_test], [0,2], transform=transforms[1])
    return mpi_sintel_plus_kitti_depth_sceneflow_train, mpi_sintel_plus_kitti_depth_sceneflow_test



def mpi_sintel_flow_plus_kitti_occ_plus_kitti_depth_sceneflow(dataset_config, root, transforms=None):
    root_folders = root.split('+')

    mpi_sintel_train, mpi_sintel_test = mpi_sintel_final(root=root_folders[0], transforms=None, split=80)

    kitti_occ_train, kitti_occ_test = kitti_occ(root=root_folders[1], transforms=None, split=80)

    kitti_depth_splits = [os.path.join(root_folders[2],'train.txt'), os.path.join(root_folders[2],'val.txt')]
    kitti_depth_train, kitti_depth_test = kitti_depth(root=root_folders[2],transforms=None, split=kitti_depth_splits)

    mpi_sintel_plus_kitti_depth_sceneflow_train = MiltiListDataset(3, [mpi_sintel_train,kitti_occ_train,kitti_depth_train], [0,0,2], transform=transforms[0])
    mpi_sintel_plus_kitti_depth_sceneflow_test = MiltiListDataset(3, [mpi_sintel_test,kitti_occ_test,kitti_depth_test], [0,0,2], transform=transforms[1])
    return mpi_sintel_plus_kitti_depth_sceneflow_train, mpi_sintel_plus_kitti_depth_sceneflow_test