# Object Detection From Images

- Object detection training should be done with mmdetection.
- We provide a custom fork of mmdetection that can export onnx model as well as the heads.

### TODO: The following instructions have changed. To be updated.

Object detections from images is one of the most important tasks in computer vision and Deep Learning. Here we shw a fe examples of training object detection models.

Our scripts use a modified version of mmdetection [3] code base for object detection. We have only done minimal modifications fot mmdetection to be able to integrate into our devkit. We sincerely thank the authors of mmdetection for integrating so many features and detectors into the code base.  

## Pascal VOC Dataset

In this training we will using the VOC2007 and VOC2012 'trainval' sets for training and the VOC2007 'test' set for validation.
Execute the following bash commands:
### Download the data
* Download the data using the following:
    ```
    mkdir /data/datasets/voc
    cd /data/datasets/voc
    wget http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar
    wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar
    wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar
    ```
### Extract the data.
* Extract the data as follows:
    ```
    tar -xvf VOCtrainval_11-May-2012.tar
    tar -xvf VOCtrainval_06-Nov-2007.tar
    tar -xvf VOCtest_06-Nov-2007.tar
    ```

### Training
* Open the file ./scripts/train_object_detection_config.py and make sure 'config_script' is set to a 'voc' config file and not a 'coco' config file.

* To start the training. run the bash command from the base folder of the repository:<br>
    ```
    python ./scripts/train_object_detection_config.py
    ```

### Evaluation
* To evaluate the detection accuracy, open the file ./scripts/test_object_detection_config.py and the 'config_script' is same as the one used for training. Also set 'checkpoint_file' to the latest check point obtained by training. Evaluation can be done by<br>
    ```
    python ./scripts/test_object_detection_config.py
    ```

## COCO 2017 Dataset
### Download
* Download the COCO 2017 dataset from http://cocodataset.org/#home
    ```
    mkdir /data/datasets/coco
    cd /data/datasets/coco
    wget http://images.cocodataset.org/zips/train2017.zip
    wget http://images.cocodataset.org/zips/val2017.zip
    wget http://images.cocodataset.org/annotations/annotations_trainval2017.zip
    ```

### Extract the files 
Extract the files to the folder /data/datasets/coco. This folder should have subfolders train2017, val2017, annotations
    ```
    unzip "*.zip"<br>
    ```

### Training & Evaluation
* Open the file ./scripts/train_object_detection_config.py and make sure 'config_script' is set to a 'coco' and not a 'voc' config file.

* To start the training. run the bash command from base folder of the repository<br>
   ```
  python ./scripts/train_object_detection_config.py
  ```

* Evaluation can be done by<br>*python ./scripts/test_object_detection_config.py*

## Cityscapes Dataset

### Download
Cityscapes dataset can be obtained from https://www.cityscapes-dataset.com/. You may need to register and get permission to download some files.

### Convert 
mmdetection supports only COCO and VOC formats for object detection. In order to use Cityscapses dataset it has to be conveted into the COCO format. 

A tool for this conversion is given along with the maskrcnn-benchmark source code: https://github.com/facebookresearch/maskrcnn-benchmark/tree/master/tools/cityscapes. The scripts that they have provided can be placed into https://github.com/mcordts/cityscapesScripts for this conversion. 

We have skipped some details, but if you have expertise in python, it should be easy to figure things out from the error messages that you get while running.


## Results


|Dataset    |Mode Name       |Backbone Model|Backbone Stride| Resolution|Complexity (GigaMACS)|MeanAP% |
|---------  |----------      |-----------   |-------------- |---------- |--------             |-------- |
|VOC2007*   |SSD             |MobileNetV1   |32             |512x512    |                     |**74.9**|
|.
|VOC2007*   |SSD[2]          |MobileNetV1   |32             |512x512    |                     |72.7     |
|VOC2007*   |SSD[1]          |VGG16         |               |512x512    |                     |76.9     |

*VOC2007+VOC2012 Train, VOC2007 validation<br><br>


|Dataset    |Mode Name       |Backbone Model|Backbone Stride| Resolution|Complexity (GigaMACS)|MeanAP[0.5:0.95]% |
|---------  |----------      |-----------   |-------------- |---------- |--------             |-------- |
|COCO2017** |SSD             |MobileNetV1   |32             |512x512    |                     |**22.2**|
|.
|COCO2017** |SSD[2]          |MobileNetV1   |32             |512x512    |                     |18.5     |
|COCO2017** |SSD[3]          |VGG16         |               |512x512    |                     |29.3     |

**COCO2017 validation set was used for validation

## References
[1] SSD: Single Shot MultiBox Detector, Wei Liu, Dragomir Anguelov, Dumitru Erhan, Christian Szegedy, Scott Reed, Cheng-Yang Fu, Alexander C. Berg, arXiv preprint arXiv:1512.02325

[2] Repository for Single Shot MultiBox Detector and its variants, implemented with pytorch, python3, https://github.com/ShuangXieIrene/ssds.pytorch

[3] Open MMLab Detection Toolbox and Benchmark, Chen, Kai and Wang, Jiaqi and Pang, Jiangmiao and Cao, Yuhang and Xiong, Yu and Li, Xiaoxiao and Sun, Shuyang and Feng, Wansen and Liu, Ziwei and Xu, Jiarui and Zhang, Zheng and Cheng, Dazhi and Zhu, Chenchen and Cheng, Tianheng and Zhao, Qijie and Li, Buyu and Lu, Xin and Zhu, Rui and Wu, Yue and Dai, Jifeng and Wang, Jingdong and Shi, Jianping and Ouyang, Wanli and Loy, Chen Change and Lin, Dahua, arXiv preprint arXiv:1906.07155, https://github.com/open-mmlab/mmdetection

[4] The PASCAL Visual Object Classes (VOC) Challenge, Everingham, M., Van Gool, L., Williams, C. K. I., Winn, J. and Zisserman, A. International Journal of Computer Vision, 88(2), 303-338, 2010, http://host.robots.ox.ac.uk/pascal/VOC/

[5] Microsoft COCO: Common Objects in Context, Tsung-Yi Lin, Michael Maire, Serge Belongie, Lubomir Bourdev, Ross Girshick, James Hays, Pietro Perona, Deva Ramanan, C. Lawrence Zitnick, Piotr Dollár, arXiv preprint arXiv:1405.0312

[6]The Cityscapes Dataset for Semantic Urban Scene Understanding, Marius Cordts, Mohamed Omran, Sebastian Ramos, Timo Rehfeld, Markus Enzweiler, Rodrigo Benenson, Uwe Franke, Stefan Roth, Bernt Schiele, CVPR 2016, https://www.cityscapes-dataset.com/


